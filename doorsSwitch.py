# 0 = yellow, 1 = red

def set_objects(i): #i is the number of objects
    objects = []
    while len(objects) < i: 
        objects.append(0)
    for num in range(1,i+1): #to change number of multiples you flip
        object_num = 0 #
        for i in objects:
            object_num += 1
            if object_num%num == 0:
                if objects[object_num-1] == 0:
                    objects[object_num-1] = 1
                elif objects[object_num-1] == 1: #change num of object states
                    objects[object_num-1] = 2
                else:
                    objects[object_num-1] = 0
    return objects

for i in range(1,101):
    print(set_objects(i).count(0), set_objects(i).count(1), set_objects(i).count(2))
