#Expii Solve, Week #1, Question 5, Climb the Ladder. You're on the first rung on a 1000 rung ladder. You roll a 6 sided die. If it's a 1 or 2, you move down one rung. If it's 3 or above, you move up one rung. If you move down from the first rung, you fall into the fire. If you move up from the 1000th rung, you escape. What is the probability you escape.

#--------------------------------------------------

#This function works by creating a list to represent the ladder, then based on a dice roll (randint between 1 and 6) move the person up or down the ladder (list). To find the probability, simulate this many times, then divide the number of times the person survives by the total number of simulation.

#--------------------------------------------------

#random used to roll the dice.
import random

#--------------------------------------------------

#The ladder will be a list of 0s, 1s and an 8. 1s represent an empty rung of the ladder, 0 at the 0th index represents the fire, 0 at the 1001th index represents safety. 8 represents the rung of the ladder the person is on.

ladder = [0,8] #The ladder starts with the fire, then the person on the first rung.

for i in range(0, 999): #This loop will add 999 rungs to the first rung with the person.
    ladder.append(1)

ladder.append(0) #This adds the safety above the first rung.

#--------------------------------------------------

#This is the function that will be used to simulate movement up and down the ladder based on the dice roll.
    
def simulate_ladder():

    #If the person is not in the fire or in safety, then roll the dice.
    if ladder.index(8) > 0 and ladder.index(8) < 1001: 
        rand = random.randint(1,6)

        #If the dice roll is 3 or greater, then move the person up one rung. 
        if rand > 2:
            ladder.insert(ladder.index(8)+1, ladder.pop(ladder.index(8)))

        #If the dice roll is 2 or less, then move the person one rung down the ladder.
        else:
            ladder.insert(ladder.index(8)-1, ladder.pop(ladder.index(8)))

    #If the person is in the fire, then move the person back to the first first rung and return False.
    elif ladder.index(8) == 0:
            ladder.insert(1, ladder.pop(ladder.index(8)))
            return False

    #If the person is in the safety, then mvoe the person back to the first rung and return True.
    elif ladder.index(8) == 1001:
        ladder.insert(1, ladder.pop(ladder.index(8)))
        return True

#--------------------------------------------------

results = [simulate_ladder() for _ in range(0,10000)] #Executes simulate_ladder function 10000 times, and store it's result in a list called results.

print("The probability of surviving is " + str(results.count(True)/100) + "%. " "To find the answer as a percent, divide the number of times the function returned true (survival) by the total number of simulations, then multiply by 100.")
