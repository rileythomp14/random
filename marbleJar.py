#Brilliant.org problem.You have a jar with 1001 blue marbles, 1000 red marbles, and 1000 green marbles. You randomly remove two marbles from the jar. If they are both red, add two blue marbles to the jar. If one is green and the other is blue or red, add a red marble. If you remove anything else, add a green marble. What will be the last marble remaining?

#--------------------------------------------------

#This question is done by creating a list that acts as a jar and filling it with letters to act as numbers. Then a while loop is used to simulate the given conditions until one marble is left, which is shown to be red.

#--------------------------------------------------
 
#random used to select a marble from the jar.
import random 

#--------------------------------------------------

#Create the jar as a list that is initially empty.
jar = [] 

#--------------------------------------------------

#This loop will add 3001 marbles to the jar (1001 blue ones, 1000 red ones, and 1000 green ones).
#This is done by adding either b, r or g to the list (1001 bs, 1000 rs, and 1000 gs).
for i in range(1,3002):
    if i %3 == 1: 
        jar.append("b") #i%3 = 1 = Blue
    if i%3 == 2:
        jar.append("r") #i%3 = 2 = Red
    if i%3 == 0:
        jar.append("g") #i%3 = 3 = Green

#--------------------------------------------------

#This loop will simulate the removal and addition of marbles from the jar.
#This loop will run until there is one marble left in the jar.

while len(jar) != 1:

    #This randomly selects two marbles from the jar (two letters from the list).
    m1 = random.choice(jar) 
    m2 = random.choice(jar)

    #Remove these two marbles from the jar (letters from the list).
    jar.remove(m1) 
    jar.remove(m2)

    #If both the marbles are red, then add two blue marbles to the jar.
    if m1 == "r" and m2 == "r": 
        jar.append("b") 
        jar.append("b")
    
    #If one of the marbles is green and the other is blue or red, then add a red marble to the other.
    elif (m1 == "g" and m2 == "b") or (m1 == "b" and m2 == "g") or (m1 == "g" and m2 == "r") or (m1 == "r" and m2 == "g"): 
        jar.append("r")

    #If the two marbles are anything else, then add a green marble.
    else: 
        jar.append("g")

#--------------------------------------------------

print(jar) #This prints [r], proving that the last marble remaining will be a red.
